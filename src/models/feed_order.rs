use serde_repr::*;

#[derive(Clone, Debug, Deserialize_repr)]
#[repr(u8)]
pub enum FeedOrder {
    None = 0,
    OldestFirst = 1,
    NewestFirst = 2,
}
