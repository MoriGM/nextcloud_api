use crate::FolderID;
use serde_derive::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize)]
pub struct Folder {
    pub id: FolderID,
    pub name: String,
}

#[derive(Debug, Serialize)]
pub struct FolderName {
    pub name: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct FoldersResponse {
    pub folders: Vec<Folder>,
}
