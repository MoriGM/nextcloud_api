use crate::models::FeedOrder;
use crate::{FeedID, FolderID, ItemID};
use serde_derive::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize)]
pub struct Feed {
    pub id: FeedID,
    pub url: String,
    pub title: String,
    #[serde(default)]
    #[serde(rename = "faviconLink")]
    pub favicon_link: Option<String>,
    pub added: i64,
    #[serde(default)]
    #[serde(rename = "folderId")]
    pub folder_id: Option<FolderID>,
    #[serde(default)]
    #[serde(rename = "unreadCount")]
    pub unread_count: Option<u64>,
    pub ordering: FeedOrder,
    #[serde(default)]
    pub link: Option<String>,
    pub pinned: bool,
    #[serde(rename = "updateErrorCount")]
    pub update_error_count: u64,
    #[serde(default)]
    #[serde(rename = "lastUpdateError")]
    pub last_update_error: Option<String>,
}

#[derive(Debug, Serialize)]
pub struct FeedCreateInput {
    pub url: String,
    #[serde(rename = "folderId")]
    pub folder_id: Option<FolderID>,
}

#[derive(Debug, Serialize)]
pub struct FeedMoveInput {
    #[serde(rename = "folderId")]
    pub folder_id: Option<FolderID>,
}

#[derive(Debug, Serialize)]
pub struct FeedRenameInput {
    #[serde(rename = "feedTitle")]
    pub feed_title: String,
}

#[derive(Debug, Serialize)]
pub struct FeedMarkReadInput {
    #[serde(rename = "newestItemId")]
    pub newest_item_id: ItemID,
}

#[derive(Clone, Debug, Deserialize)]
pub struct FeedsResponse {
    #[serde(default)]
    #[serde(rename = "starredCount")]
    pub starred_count: Option<u64>,
    #[serde(default)]
    #[serde(rename = "newestItemId")]
    pub newest_item_id: Option<i64>,
    pub feeds: Vec<Feed>,
}
