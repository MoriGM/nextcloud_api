use serde_repr::*;

#[derive(Clone, Debug, Serialize_repr)]
#[repr(u8)]
pub enum ItemType {
    Feed = 0,
    Folder = 1,
    Starred = 2,
    All = 3,
    Shared = 4,
    Explore = 5,
    Unread = 6,
}
