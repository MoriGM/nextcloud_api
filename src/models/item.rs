use super::ItemType;
use crate::{FeedID, ItemID};
use serde_derive::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize)]
pub struct Item {
    pub id: ItemID,
    pub guid: String,
    #[serde(rename = "guidHash")]
    pub guid_hash: String,
    #[serde(default)]
    pub url: Option<String>,
    #[serde(default)]
    pub title: Option<String>,
    pub author: Option<String>,
    #[serde(rename = "pubDate")]
    pub pub_date: i64,
    pub body: String,
    #[serde(default)]
    #[serde(rename = "enclosureMime")]
    pub enclosure_mime: Option<String>,
    #[serde(default)]
    #[serde(rename = "enclosureLink")]
    pub enclosure_link: Option<String>,
    #[serde(default)]
    #[serde(rename = "mediaThumbnail")]
    pub media_thumbnail: Option<String>,
    #[serde(default)]
    #[serde(rename = "mediaDescription")]
    pub media_description: Option<String>,
    #[serde(rename = "feedId")]
    pub feed_id: FeedID,
    pub unread: bool,
    pub starred: bool,
    pub rtl: bool,
    #[serde(rename = "lastModified")]
    pub last_modified: i64,
    pub fingerprint: String,
}

#[derive(Debug, Serialize)]
pub struct ItemQueryInput {
    #[serde(rename = "batchSize")]
    pub batch_size: i64,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub offset: Option<u64>,
    #[serde(rename = "type")]
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub item_type: Option<ItemType>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<u64>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "getRead")]
    pub get_read: Option<bool>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "oldestFirst")]
    pub oldest_first: Option<bool>,
}

#[derive(Debug, Serialize)]
pub struct ItemUpdatedQueryInput {
    #[serde(rename = "lastModified")]
    pub last_modified: u64,
    #[serde(rename = "type")]
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub item_type: Option<ItemType>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<u64>,
}

#[derive(Debug, Serialize)]
pub struct ItemMarkInput {
    #[serde(rename = "itemIds")]
    pub item_ids: Vec<ItemID>,
}

#[derive(Debug, Serialize)]
pub struct ItemMarkAllInput {
    #[serde(rename = "newestItemId")]
    pub newest_item_id: ItemID,
}

#[derive(Clone, Debug, Deserialize)]
pub struct ItemsResponse {
    pub items: Vec<Item>,
}
