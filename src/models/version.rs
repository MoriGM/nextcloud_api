use serde_derive::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct Version {
    pub version: String,
}
