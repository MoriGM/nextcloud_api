use reqwest::StatusCode;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ApiError {
    #[error("Failed to Parse URL")]
    Url(#[from] url::ParseError),
    #[error("Failed to (de)serialize Json")]
    Json {
        source: serde_json::error::Error,
        json: String,
    },
    #[error("Http request failed")]
    Http(#[from] reqwest::Error),
    #[error("Error parsing string (enum/date) returned by miniflux")]
    Parse,
    #[error("Invalid Input")]
    Input,
    #[error("Unauthorized")]
    Unauthorized,
    #[error("Unknown error")]
    Unknown,
}

impl From<StatusCode> for ApiError {
    fn from(status: StatusCode) -> Self {
        match status {
            StatusCode::UNAUTHORIZED => ApiError::Unauthorized,
            _ => ApiError::Unknown,
        }
    }
}
