use crate::{models::ItemType, NextcloudNewsApi};
use reqwest::Client;
use std::env;
use url::Url;

fn test_setup_env() -> NextcloudNewsApi {
    dotenv::dotenv().expect("Failed to read .env file");
    let url = env::var("NEXTCLOUD_URL").expect("Failed to read NEXTCLOUD_URL");
    let user = env::var("NEXTCLOUD_USER").expect("Failed to read NEXTCLOUD_USER");
    let pw = env::var("NEXTCLOUD_PW").expect("Failed to read NEXTCLOUD_PW");

    let url = Url::parse(&url).unwrap();
    NextcloudNewsApi::new(&url, user, pw).unwrap()
}

#[tokio::test(flavor = "current_thread")]
async fn version() {
    let api = test_setup_env();
    let version = api.get_version(&Client::new()).await.unwrap();
    println!("{:?}", version);
}

#[tokio::test(flavor = "current_thread")]
async fn folders() {
    let api = test_setup_env();
    let folders = api.get_folders(&Client::new()).await.unwrap();
    println!("{:?}", folders);
}

#[tokio::test(flavor = "current_thread")]
async fn create_folder() {
    let api = test_setup_env();
    let folder = api.create_folder(&Client::new(), "test 2").await.unwrap();
    println!("{:?}", folder);
}

#[tokio::test(flavor = "current_thread")]
async fn delete_folder() {
    let api = test_setup_env();
    api.delete_folder(&Client::new(), 4).await.unwrap();
}

#[tokio::test(flavor = "current_thread")]
async fn feeds() {
    let api = test_setup_env();
    let feeds = api.get_feeds(&Client::new()).await.unwrap();
    println!("{:?}", feeds);
}

#[tokio::test(flavor = "current_thread")]
async fn create_feed() {
    let api = test_setup_env();
    let feed = api
        .create_feed(
            &Client::new(),
            "https://www.heise.de/rss/heise-Rubrik-Entertainment-atom.xml",
            None,
        )
        .await
        .unwrap();
    println!("{:?}", feed);
}

#[tokio::test(flavor = "current_thread")]
async fn items() {
    let api = test_setup_env();
    let items = api
        .get_items(&Client::new(), 10, None, None, None, Some(false), None)
        .await
        .unwrap();
    println!("{:?}", items);
}

#[tokio::test(flavor = "current_thread")]
async fn starred_items() {
    let api = test_setup_env();
    let starred_items = api
        .get_items(
            &Client::new(),
            2,
            None,
            Some(ItemType::Starred),
            None,
            None,
            None,
        )
        .await
        .unwrap();
    println!("items: {:?}", starred_items);
}
